var $ = require('jquery');
let {get, post, del, put} = require('RestHelper');

module.exports = {
  GetAircraftFromDB: function () {
    return new Promise((resolve, reject) =>
    {
        get('/getAircraftList').then((res) => {
        if(res === undefined || res == null || res == "undefined"){
          reject("not found")
        } else {
          resolve(res);
        }
      }).catch((err) => {
        reject(err);
      });
    })
  }
};

  //   var Aircraft = [
  //     {
  //       key:'1',
  //       value:'B737',
  //       string:'Boeing 737'
  //   },
  //     {
  //       key:'2',
  //       value:'B744',
  //       string:'Boeing 747-400'
  //     },
  //     {
  //       key:'3',
  //       value:'B772',
  //       string:'Boeing 777-200'
  //     }
  //   ];
  //   return Aircraft;
  // }
