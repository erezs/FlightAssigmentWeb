//Handle the material-ui click events bug
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();
//React + React stormpath
import React from 'react';
import { render } from 'react-dom';
import { Router,IndexRoute, Route, hashHistory ,browserHistory } from 'react-router';

//material-ui
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

// Components
import Main from 'Main';
import Dashboard from 'Dashboard';
import FlightLog from 'FlightLog';
import Assignment from 'Assignment';
import login from 'login';
import logout from 'logout';
import Register from 'Register';
import ProfilePage from 'ProfilePage';
import auth from 'auth';

// App css
require('style!css!sass!applicationStyles')

function requireAuth(nextState, replace) {
  if (!auth.loggedIn()){
    replace('/login')
  }
}

render(
  <Router history={browserHistory}>
    <Route path="/" component={Main} >
      <IndexRoute component={Dashboard} onEnter={requireAuth}/>
      <Route path='FlightLog' component={FlightLog} onEnter={requireAuth} />
      <Route path='Assignment' component={Assignment} onEnter={requireAuth} />
      <Route path='login' component={login} />
      <Route path="logout" component={logout} />
      <Route path='Register' component={Register}/>
    </Route>
  </Router>,
  document.getElementById('app')
);
