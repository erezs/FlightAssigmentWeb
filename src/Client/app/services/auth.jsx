let $ = require('jquery');
let {get, post, del, put} = require('RestHelper');

var auth = {
  login(email, pass, cb) {
    cb = arguments[arguments.length - 1];
    post('/login', {email: email, password: pass})
    .then((data) => {
      sessionStorage.token = Math.random().toString(36).substring(7);
      if (cb) cb(true)
      this.onChange(true)
    })
    .catch((err) => {
      if (cb) cb(false)
      this.onChange(false)
    });
  },
  signup(email,pass,cb) {
    post('/signup',{email:email,password:pass})
    .then((data) => {
      sessionStorage.token = Math.random().toString(36).substring(7);
      if (cb) cb(true)
      this.onChange(true)
    })
    .catch((err) => {
      if (cb) cb(false)
      this.onChange(false)
    });
  },
  getToken() {
    return (typeof window !== "undefined") ? sessionStorage.token : undefined;
  },
  logout(cb) {
    post('/logout')
    .then((g) => {
      sessionStorage.token = undefined;
      if (cb) cb()
      this.onChange(false)
    }).catch((err) => {
      console.log(err);
    });
  },
  getLoggedInUser() {
    return new Promise((resolve, reject) =>
    {
      get('/getLoggedInUser').then((res) => {
        // if(res.username.localemail === undefined || res.username.localemail == null || res.username.localemail == "undefined"){
        if(res.username === undefined || res.username == null || res.username == "undefined"){
          reject("not found")
        } else {
          sessionStorage.token = Math.random().toString(36).substring(7);
          resolve(res.username.localemail);
        }
      }).catch((err) => {
        reject(err);
      });
    })
  },
  loggedIn() {
    var token = sessionStorage.getItem('token');
    if (token === undefined || token == null || token == "undefined"){
      return false;
    } else {
      return true;
    }
  },
  onChange() {}
}
module.exports = auth;
