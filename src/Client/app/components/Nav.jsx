import React from 'react';
import { browserHistory, Router, Route, Link, withRouter } from 'react-router'

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton'
import auth from 'auth';

class Nav extends React.Component {
  constructor(props) {
    super(props);
    this.state = {loggedIn: auth.loggedIn()}
  }

  onSearch(e) {
    e.preventDefault();
    alert('Not yet working!');
  }

  updateAuth(loggedIn) {
    this.setState = {loggedIn: loggedIn}
  }

  componentDidMount() {
    auth.onChange = this.updateAuth
    // auth.login()
  }

  handleTouchTap(task) {
    ActionCreator.openNav();
  }

  render() {
    //Get the loggedin state
    var loggedIn = auth.loggedIn();
    //Init the variables
    var loginButton,AssignmentButton,FlightLogButton,RegisterButton;
    //Check if we're loggedin, based on the state we show / hide the buttons
    if (loggedIn){
      loginButton = (<li><Link to="/logout">Log out</Link></li>);
      AssignmentButton = (<li><Link to="Assignment" activeClassName="active" activeStyle={{fontWeight: 'bold'}}>Get Flight Assignment</Link></li>);
      FlightLogButton = (<li><Link to="FlightLog" activeClassName="active" activeStyle={{fontWeight: 'bold'}}>Flight Log</Link></li>);
      RegisterButton = (<span></span>);
    } else {
      loginButton = (<li><Link to="/login">Sign in</Link></li>);
      AssignmentButton = (<span></span>);
      FlightLogButton = (<span></span>);
      RegisterButton = (<li><Link to="/Register">Register</Link></li>);
    };
    return (
      <div>
        <nav className="light-blue lighten-1" role="navigation">
          <div className="nav-wrapper container">
            <Link to="/" className="brand-logo">Flights App</Link>
            <ul className="right">
              {AssignmentButton}
              {FlightLogButton}
              {loginButton}
              {RegisterButton}
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default Nav;
