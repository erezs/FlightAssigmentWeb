import React from 'react';
import { browserHistory } from 'react-router'

import auth from 'auth';

class Logout extends React.Component {
  componentDidMount() {
    sessionStorage.clear();
    auth.logout();
    //this.props.router.push('/');
    setTimeout(function() { browserHistory.push('/');}, 2000);
  }

  render() {
    return (
      <h1 className="center-align">You are now being logged out</h1>

    )
  }
}

export default Logout
