import React from 'react';
import { browserHistory, Router, Route, Link, withRouter } from 'react-router'
import auth from 'auth';

class Dashboard extends React.Component {
  constructor(props, context){
    super(props, context);
    this.state = {};
    auth.getLoggedInUser().then(res =>{
      this.setState({user : res});
    }).catch(
      err => {
        console.log(err);
        browserHistory.push('/login');
      }
    )
  }

  render() {
    const token = auth.getToken();
    return (
      <div>
        <h3>Dashboard</h3>
        <p>Welcome {this.state.user}</p>
      </div>
    )
  };
}

export default Dashboard;
