import React from 'react';
import DocumentTitle from 'react-document-title';
import { UserComponent } from 'react-stormpath';

class ProfilePage extends UserComponent {
  render() {
    return (
      <DocumentTitle title={`My Profile`}>
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h3>My Profile</h3>
              <hr />
            </div>
          </div>
          <div className="row">
            <h1>User: {this.props.params.id}</h1>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}
export default ProfilePage;
