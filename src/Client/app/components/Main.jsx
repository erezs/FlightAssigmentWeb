import React from 'react';
import { Link } from 'react-router';
import DocumentTitle from 'react-document-title';
import {cyan500} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Nav from 'Nav';
import auth from 'auth';

const styles = {
  container: {
    textAlign: 'center',
    paddingTop: 200,
  },
};

const muiTheme = getMuiTheme({
  palette: {
    textColor: cyan500,
  },
});

class Main extends React.Component {
  constructor() {
    super();
  }
  render () {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        <div>
          <Nav/>
          <div className="section no-pad-bot">
            <div className="container">
              <div className="section">
                <div className="row">
                  {this.props.children}
                </div>
              </div>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default Main;
