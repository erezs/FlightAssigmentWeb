import React from 'react'
import { browserHistory } from 'react-router'
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import auth from 'auth'

class Register extends React.Component {
  constructor(props, context){
    super(props, context);
    this.state = {};
    this.state.error = "";
    this.state.email = "";
    this.state.password = "";
    this.history = props.history;
    this.showSessionMsg = props.location.query? props.location.query.session:true;
    this._handlePasswordChange = this._handlePasswordChange.bind(this);
    this._handleEmailChange = this._handleEmailChange.bind(this);
    this._formSubmit = this._formSubmit.bind(this);
  }
  _handleEmailChange(e){
    this.state.errorEmail = "";
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if(!e.target.value){
      this.state.errorEmail = "This field is required.";
    }else if(!re.test(e.target.value)){
      this.state.errorEmail = "Email is not valid.";
    }
    this.setState({errorEmail : this.state.errorEmail});
    this.setState({email : e.target.value});
  }
  _handlePasswordChange(e){
    this.state.errorPassword = "";
    if(!e.target.value){
      this.state.errorPassword = "This field is required.";
    } else if(e.target.value.length < 6){
      this.state.errorPassword = "Password needs more than 6 characters.";
    }
    this.setState({errorPassword : this.state.errorPassword});
    this.setState({password : e.target.value});

  }

  _formSubmit(e) {
    e.preventDefault();
    if(this.state.errorPassword == '' && this.state.errorEmail == ''){
      //Lower case the email if someone enters Gmail instead of gmail
      this.state.email = this.state.email.toLowerCase();
      auth.signup(this.state.email, this.state.password, (loggedIn) => {
        if (!loggedIn){
          return this.setState({ error: "Incorrect Email or password" })
        } else {
          browserHistory.push('/');
        }
      })
    }
  }
  render() {
    return (
      <div className="col s12 m4 l8 offset-l2 offset-m4 offset-s4">
        <Paper zDepth={3} className="mediaPiece">
          <div style={{padding:15}}>
            <h2 className="center-align">
              Registration
            </h2>
            <form className="center-align" onSubmit={this._formSubmit}>
              <TextField
                hintText="Enter Email"
                errorText={this.state.errorEmail}
                floatingLabelText="Email"
                onChange={this._handleEmailChange}
                value={this.state.email}
                />
              <br/>
              <TextField
                hintText="Enter Password"
                errorText={this.state.errorPassword}
                floatingLabelText="Password"
                onChange={this._handlePasswordChange}
                value={this.state.password}
                type="password"
                />
              <br/>
              <RaisedButton
                label="Register"
                secondary={true}
                onClick={this._formSubmit}/>
              <p>
                {this.state.error}
              </p>
            </form>
          </div>
        </Paper>
        <br/>
      </div>
    );
  }
}

export default Register
