import React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Card from 'material-ui/Card/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';


class AssignmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Aircraft:this.props.aircrafts[0].ICAO_CODE,
      Legs:1,
      Haul:"short"
    };
  };

  //Helpers to save the value into state which is read from the select component
  _handleChangeHaul = (event, index, value) => this.setState({Haul:value});
  _handleChangeAircraft = (event,index,value) => this.setState({Aircraft:value});
  _handleChangeLegs = (events,index,value) => this.setState({Legs:value});

  render() {
    const styles = {
      block: {
        maxWidth: 250,
      },
      radioButton: {
        marginBottom: 16,
      },
      card: {
        padding: 10,
        margin: 40,
      },
      RaisedButton: {
        margin: 12
      },
      div: {
        marginTop: 12
      }
    };


    //Get the of aircraft
    var MenuItems = [];

    this.props.aircrafts.forEach(function(aircraft) {
      MenuItems.push(<MenuItem value={aircraft.ICAO_CODE} primaryText={aircraft.NAME} key={aircraft.id} />)
    });

    return (
      <div className="col s12 m12 center">
        <h1 className="center">Flight Assignment</h1>
        <h4>Flight Length</h4>
        <SelectField value={this.state.Haul} onChange={this._handleChangeHaul}>
          <MenuItem value="short" primaryText="Short Haul (0-3 Hours)" />
          <MenuItem value="medium" primaryText="Medium Haul (3-6 Hours)" />
          <MenuItem value="long" primaryText="Long Haul(6+ Hours)" />
        </SelectField>
        <h4>Aircraft</h4>
        <SelectField value={this.state.Aircraft} onChange={this._handleChangeAircraft}>
          {MenuItems}
        </SelectField>
        <h4>No. of Legs</h4>
        <p>Select the numbers of legs you would like to be assigned</p>
        <SelectField value={this.state.Legs} onChange={this._handleChangeLegs}>
          <MenuItem value={1} primaryText="1" />
          <MenuItem value={2} primaryText="2" />
          <MenuItem value={3} primaryText="3" />
          <MenuItem value={4} primaryText="4" />
          <MenuItem value={5} primaryText="5" />
        </SelectField>
        <div style={{marginTop:15}}>
          <RaisedButton label="Generate" primary={true} style={styles} />
        </div>
      </div>
    )
  }
}

export default AssignmentForm;
