import React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Card from 'material-ui/Card/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import FlightAssignAPI from 'FlightAssignAPI';
import AssignmentForm from 'AssignmentForm';

const styles = {
  customWidth: {
    width: 150,
  },
};


class Assignment extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      aircrafts: null
    }
  }
  componentWillMount(){
    this.loadData();
  }

  loadData(){
    FlightAssignAPI.GetAircraftFromDB().then(res =>{
      this.setState({aircrafts : res});
    }).catch(
      err => {
        console.log(err);
      }
    )
  }

  render() {
    //http://stackoverflow.com/questions/35022922/how-can-i-block-a-react-component-to-be-rendered-until-i-fetched-all-information/35023545#35023545
    if (!this.state.aircrafts){
      return <div/>
    }

    return (
      <div>
        <AssignmentForm aircrafts={this.state.aircrafts}/>
      </div>
    )
  }
}

export default Assignment;
