module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Aircraft',{
    ICAO_CODE: {
      type: DataTypes.STRING,
      allowNull: false
    },
    NAME: {
      type: DataTypes.STRING,
      allowNull: false
    },
    REGISTRATION: {
      type: DataTypes.STRING,
      allowNull: true
    }
  });
};
