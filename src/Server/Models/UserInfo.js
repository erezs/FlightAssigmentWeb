module.exports = function(sequelize, DataTypes) {
  return sequelize.define('UserInfo',{
    DEP_ICAO: {
      type: DataTypes.STRING,
      allowNull: false
    },
    ARR_ICAO: {
      type: DataTypes.STRING,
      allowNull: false
    },
    AC_TYPE: {
      type: DataTypes.STRING,
      allowNull: false
    },
    DATE: {
      type: DataTypes.DATE,
      allowNull: false
    },
    COMPLETED: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    USER_ID: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  });
};
