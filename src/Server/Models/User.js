
var bcrypt   = require('bcrypt-nodejs');


module.exports = function(sequelize, DataTypes) {
  return sequelize.define('User', {
		localemail			: DataTypes.STRING,
		localpassword		: DataTypes.STRING
	},
	{
		classMethods: {
			generateHash : function(password) {
				return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
			},
		},
		instanceMethods: {
			validPassword : function(password) {
				return bcrypt.compareSync(password, this.localpassword);
			}
		},
		getterMethods: {
			currSomeValue: function() {
				return this.someValue;
			}
		},
		setterMethods: {
			newSomeValue: function(value) {
				this.someValue = value;
			}
		}
	});
};
