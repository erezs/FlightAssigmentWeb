module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Destination',{
    ICAO: {
      type: DataTypes.STRING,
      allowNull: false
    },
    NAME: {
      type: DataTypes.STRING,
      allowNull: false
    },
    DEP_TIME: {
      type: DataTypes.STRING,
      allowNull: false
    },
    DURATION: {
      type: DataTypes.STRING,
      allowNull: false
    },
    AIRCRAFT_TYPE: {
      type: DataTypes.STRING,
      allowNull: false
    },
    CARGO: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
  });
};
