var Sequelize = require('sequelize');
var sequelize_fixtures = require('sequelize-fixtures');

var env = process.env.NODE_ENV || 'development';
var sequelize;

if (env === 'production') {
	sequelize = new Sequelize(process.env.DATABASE_URL, {
		dialect: 'postgres'
	});
} else {
	sequelize = new Sequelize(undefined, undefined, undefined, {
		'dialect': 'sqlite',
		'storage': __dirname + '/Data/Flight.db'
	});
}

var db = {};


db.Aircraft = sequelize.import(__dirname + '/Models/Aircraft.js');
db.Destination = sequelize.import(__dirname + '/Models/Destination.js');
db.UserInfo = sequelize.import(__dirname + '/Models/UserInfo.js');
db.User = sequelize.import(__dirname + '/Models/User.js');


db.sequelize = sequelize;
db.Sequelize = Sequelize;

var models  = {Destination: db.Destination,Aircraft:db.Aircraft};

db.sequelize.sync().then(function(){
	sequelize_fixtures.loadFile(__dirname + '/Data/*.json', models).then(function(){
		console.dir("=================================");
		console.dir("DATA CREATED SUCCESSFULLY ");
		console.dir("=================================");
	});

});

module.exports = db;
