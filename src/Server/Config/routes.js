/*jshint esversion: 6 */

module.exports = function(app, passport,Aircraft) {
  // Passport Authentication Related ==============================
  // =============================================================================
  // LOGOUT ==============================
  app.post('/logout', function(req, res) {
    req.logOut();
    req.session.destroy(function (err) {
      res.sendStatus(200);
    });
  });
  // =============================================================================
  // AUTHENTICATE (FIRST LOGIN) ==================================================
  // =============================================================================

  app.post('/login',function (req,res,next) {
    // Do email and password validation for the server
    passport.authenticate('local-login', (authErr, user, info) => {
      if (authErr) return next(authErr);
      if (!user) {
        return res.status(401).json({ message: info.message });
      }
      // Passport exposes a login() function on req (also aliased as
      // logIn()) that can be used to establish a login session
      return req.logIn(user, (loginErr) => {
        if (loginErr) return res.status(401).json({ message: loginErr });
        // console.log(req.user);
        return res.status(200).json({
          message: 'You have been successfully logged in.'
        });
      });
    })(req, res, next);
  });

  // SIGNUP =================================
  // process the signup form
  app.post('/signup', passport.authenticate('local-signup', {
    successRedirect : '/', // redirect to the secure profile section
    failureRedirect : '/', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
  }));

  //Send the loggedin information back to the client
  app.get('/getLoggedInUser',function(req,res){
    return res.json({username:req.user});
  });


  // route middleware to ensure user is logged in
  function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
    return next();
    res.redirect('/');
  }
  // =============================================================================
  // =============================================================================
  // FlightAPI Releated ==============================
  // =============================================================================
  app.get('/getAircraftList',function (req,res){
    Aircraft.findAll().then(function(aircrafts){
      if (aircrafts.length){
        res.json(aircrafts);
      }
    }, function(e){
      res.send(500);
    });
  });
};
