var webpack = require('webpack');

module.exports = {
  entry: [
    'script!jquery/dist/jquery.min.js',
    './src/Client/app/app.jsx'
  ],
  externals: {
    jquery: 'jQuery'
  },
  plugins: [
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery'
    })
  ],
  output: {
    path: __dirname,
    filename: './src/Client/Public/bundle.js'
  },
  resolve: {
    root: __dirname,
    alias: {
      applicationStyles: 'src/Client/app/styles/app.scss',
      routes: 'src/Client/app/config/routes.jsx',
      auth: 'src/Client/app/services/auth.jsx',
      RestHelper: 'src/Client/app/services/RestHelper.jsx',
      Main: 'src/Client/app/components/Main.jsx',
      Landing: 'src/Client/app/components/Landing.jsx',
      Assignment: 'src/Client/app/components/Assignment.jsx',
      Nav: 'src/Client/app/components/Nav.jsx',
      Dashboard: 'src/Client/app/components/Dashboard.jsx',
      login: 'src/Client/app/components/LoginPage.jsx',
      logout: 'src/Client/app/components/Logout.jsx',
      Register: 'src/Client/app/components/RegistrationPage.jsx',
      ProfilePage: 'src/Client/app/components/ProfilePage.jsx',
      FlightLog: 'src/Client/app/components/FlightLog.jsx'
    },
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'stage-0']
        },
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  },
  devtool: 'cheap-module-eval-source-map'
};
