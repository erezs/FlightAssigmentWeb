/*jshint esversion: 6 */
var express = require('express');
var passport = require('passport');
var morgan = require('morgan'); // logger
//pass passport for configuration
require('./src/Server/Config/passport.js')(passport);
var React = require('react');
var Router = require('react-router').Router;

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var db = require('./src/Server/db.js');
var User = db.User;
var Aircraft = db.Aircraft;

var FlightAPI = require('./src/Server/Controllers/FlightAPI.js');

//Config
const PORT = process.env.PORT || 3000;
const ENV = process.env.NODE_ENV || 'development';

// Create our app
var app = express();

app.use(morgan('dev')); // log every request to the console
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({ secret: 'zomaareenstukjetekstDatjasdasdasdasdenietzomaarbedenkt',resave:true,saveUninitialized:false }));
app.use(passport.initialize());
app.use(passport.session());
require('./src/Server/Config/routes.js')(app, passport,Aircraft); // load our routes and pass in our app and fully configured passport

app.use(express.static('./src/Client/public'));

app.get('*', function(req, res) {
    res.sendFile(__dirname + '/src/Client/public/index.html');
});

db.sequelize.sync().then(function() {
  app.listen(PORT,function (err) {
    if (err){
      return console.log(err);
    }
    console.log('--------------------------');
    console.log('===> 😊  Starting Server . . .');
    console.log(`===>  Environment: ${ENV}`);
    console.log(`===>  Listening on port: ${PORT}`);
    if (ENV === 'production') {
      console.log('===> 🚦  Note: In order for authentication to work in production');
      console.log('===>           you will need a secure HTTPS connection');
      //sess.cookie.secure = true; // Serve secure cookies
    }
    console.log('--------------------------');
  });
});
